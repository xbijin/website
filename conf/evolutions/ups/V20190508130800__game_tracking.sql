-- Tracks game instances

CREATE TABLE IF NOT EXISTS games
(
    id        INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    season    INT                NOT NULL,
    session   INT                NOT NULL,
    home_team INT                NOT NULL,
    away_team INT                NOT NULL,
    completed BOOL               NOT NULL DEFAULT FALSE,
    UNIQUE INDEX (season, session, home_team),
    UNIQUE INDEX (season, session, away_team),
    FOREIGN KEY fk_game_home_team (home_team) REFERENCES teams (id),
    FOREIGN KEY fk_game_away_team (away_team) REFERENCES teams (id)
);

CREATE TABLE game_actions
(
    id                      INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    previous_play           INT                NULL     DEFAULT NULL,
    game_id                 INT                NOT NULL,
    play_type               INT                NOT NULL DEFAULT 0, -- 0 for state set, 1 for swing, 2 for auto K, 3 for auto BB, 4 for bunt, 5 for infield in
    pitcher                 INT                NULL     DEFAULT NULL,
    pitch                   INT                NULL     DEFAULT NULL,
    batter                  INT                NULL     DEFAULT NULL,
    swing                   INT                NULL     DEFAULT NULL,
    result_player_on_first  INT                NULL     DEFAULT NULL,
    result_player_on_second INT                NULL     DEFAULT NULL,
    result_player_on_third  INT                NULL     DEFAULT NULL,
    result_outs             INT                NOT NULL DEFAULT 0,
    result_score_away       INT                NOT NULL DEFAULT 0,
    result_score_home       INT                NOT NULL DEFAULT 0,
    result_inning           FLOAT              NOT NULL DEFAULT 0,
    CHECK ( play_type >= 0 AND play_type <= 5),
    CHECK ( (play_type = 0 AND pitcher IS NULL) OR (pitcher IS NOT NULL) ),
    CHECK ( (play_type IN (0, 2, 3) AND pitch IS NULL) OR (pitch IS NOT NULL) ),
    CHECK ( (play_type = 0 AND batter IS NULL) OR (batter IS NOT NULL) ),
    CHECK ( (play_type IN (0, 2, 3) AND swing IS NULL) OR (swing IS NOT NULL) ),
    FOREIGN KEY fk_game_action_previous_play (previous_play) REFERENCES game_actions (id),
    FOREIGN KEY fk_game_action_game (game_id) REFERENCES games (id),
    FOREIGN KEY fk_game_action_pitcher (pitcher) REFERENCES players (id),
    FOREIGN KEY fk_game_action_batter (batter) REFERENCES players (id),
    FOREIGN KEY fk_game_action_player_on_first (result_player_on_first) REFERENCES players (id),
    FOREIGN KEY fk_game_action_player_on_second (result_player_on_second) REFERENCES players (id),
    FOREIGN KEY fk_game_action_player_on_third (result_player_on_third) REFERENCES players (id)
);
