package model

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.Tables.{PitchingBonuses, _}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.MySQLProfile
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class FBDatabase @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[MySQLProfile] {

  private def awaitDB[R](awaitable: DBIOAction[R, NoStream, Nothing]): R = {
    Await.result(db.run(awaitable), Duration(5, TimeUnit.SECONDS))
  }

  def getUserByRedditName(reddit: String): Option[UsersRow] = awaitDB {
    Users.filter(_.redditName === reddit).take(1).result.headOption
  }

  def getPlayerByRedditName(reddit: String): Option[PlayersRow] = awaitDB {
    (for {
      u <- Users if u.redditName === reddit
      p <- Players if p.user === u.id
    } yield p).take(1).result.headOption
  }

  def getPlayerByName(name: String): Option[PlayersRow] = awaitDB {
    Players.filter(_.name === name).take(1).result.headOption
  }

  def getBattingTypeByShortcode(code: String): Option[BattingTypesRow] = awaitDB {
    BattingTypes.filter(_.shortcode === code).take(1).result.headOption
  }

  def getPitchingTypeByShortcode(code: String): Option[PitchingTypesRow] = awaitDB {
    PitchingTypes.filter(_.shortcode === code).take(1).result.headOption
  }

  def getPitchingBonusByShortcode(code: String): Option[PitchingBonusesRow] = awaitDB {
    PitchingBonuses.filter(_.shortcode === code).take(1).result.headOption
  }

  def createPlayerApplication(application: NewPlayerForm): PlayerApplicationsRow = awaitDB {
    val bt = getBattingTypeByShortcode(application.battingType).get.id
    val pt = application.pitchingType.map(pt => getPitchingTypeByShortcode(pt).get.id)
    val pb = application.pitchingBonus.map(pb => getPitchingBonusByShortcode(pb).get.id)
    PlayerApplications returning PlayerApplications.map(_.id) into ((app, newId) => app.copy(id = newId)) += PlayerApplicationsRow(0, application.requestedName, application.returning, application.willingToJoinDiscord, application.positionPrimary, application.positionSecondary, application.rightHanded, bt, pt, pb)
  }

  /**
    * Should only be used when the ID is expected to return a user.
    * Not to be used for arbitrary user/player lookup.
    *
    * @param id The ID of the user.
    * @return The User instance
    */
  def getUserById(id: Int): UsersRow = awaitDB {
    Users.filter(u => u.id === id).take(1).result.head
  }

  def createUserAccount(redditName: String): Int = awaitDB {
    Users returning Users.map(_.id) += UsersRow(0, redditName)
  }

}
