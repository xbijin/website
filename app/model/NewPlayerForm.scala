package model

import play.api.data.Forms._
import play.api.data._

case class NewPlayerForm(returning: Boolean, requestedName: String, willingToJoinDiscord: Boolean, positionPrimary: String, positionSecondary: Option[String], rightHanded: Boolean, battingType: String, pitchingType: Option[String], pitchingBonus: Option[String], agreeToTerms: Boolean) {

  def validateNameIsUnique()(implicit db: FBDatabase): Boolean = {
    db.getPlayerByName(requestedName).isEmpty
  }

  def validatePositions(): Boolean = {
    if (positionPrimary == "P")
      positionSecondary.isEmpty
    else
      positionSecondary.exists { pos =>
        NewPlayerForm.secondaryPositionMap
          .getOrElse(positionPrimary, Set())
          .contains(pos)
      }
  }

  def validatePlayerTypes()(implicit db: FBDatabase): Boolean = {
    db.getBattingTypeByShortcode(battingType).isDefined && {
      if (positionPrimary != "P")
        pitchingType.isEmpty &&
          pitchingBonus.isEmpty
      else
        pitchingType.exists { t => db.getPitchingTypeByShortcode(t).isDefined } &&
          pitchingBonus.exists { t => db.getPitchingBonusByShortcode(t).isDefined }
    }
  }

}

object NewPlayerForm {

  private val availablePositions: Set[String] = Set(
    "P",
    "C",
    "1B",
    "2B",
    "3B",
    "SS",
    "LF",
    "CF",
    "RF"
  )
  private val secondaryPositionMap: Map[String, Set[String]] = Map(
    "C" -> Set("1B", "3B"),
    "1B" -> Set("3B", "LF"),
    "2B" -> Set("SS", "1B", "RF"),
    "3B" -> Set("2B", "SS", "LF"),
    "SS" -> Set("2B", "3B", "CF"),
    "LF" -> Set("RF", "1B"),
    "CF" -> Set("LF", "RF"),
    "RF" -> Set("CF", "1B")
  )

  def newPlayerForm(implicit db: FBDatabase) = Form(
    mapping(
      "returning" -> checked("Are you a returning player?"),
      "requestedName" -> nonEmptyText(minLength = 5, maxLength = 60),
      "willingToJoinDiscord" -> checked("Are you willing to join the Discord community?"),
      "positionPrimary" -> nonEmptyText(minLength = 1, maxLength = 2).verifying(pos => availablePositions.contains(pos)),
      "positionSecondary" -> optional(nonEmptyText(minLength = 1, maxLength = 2)),
      "rightHanded" -> boolean,
      "battingType" -> nonEmptyText(minLength = 1, maxLength = 2),
      "pitchingType" -> optional(nonEmptyText(minLength = 1, maxLength = 2)),
      "pitchingBonus" -> optional(nonEmptyText(minLength = 1, maxLength = 2)),
      "agreeToTerms" -> checked("By completing this form, you agree to try your best to be active and participate in the game. You may be drafted by an MLR team that you are not a fan of. Do you agree to still participate in the game regardless of MLB fandom?").verifying("You must agree to these terms.", _ == true)
    )(NewPlayerForm.apply)(NewPlayerForm.unapply)
      .verifying("That name is already taken.", _.validateNameIsUnique())
      .verifying("That is not a valid secondary position for the selected primary position.", _.validatePositions())
      .verifying("Your batting/pitching types are not valid.", _.validatePlayerTypes())
  )
}
