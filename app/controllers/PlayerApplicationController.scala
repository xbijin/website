package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import model.NewPlayerForm.newPlayerForm
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class PlayerApplicationController @Inject()(implicit db: FBDatabase, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def showNewPlayerForm: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Ok(views.html.players.newplayer(newPlayerForm))
  }

  def createNewPlayer: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    newPlayerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.players.newplayer(formWithErrors)),
      formData => {
        db.createPlayerApplication(formData)
        Redirect(routes.PlayerApplicationController.showNewPlayerForm()) // TODO redirect to a viewing page for the application
      }
    )
  }

}
