package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabase
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}

@Singleton
class HomeController @Inject()(db: FBDatabase, messagesActionBuilder: MessagesActionBuilder,  cc: ControllerComponents) extends AuthenticatedController(db, messagesActionBuilder, cc) {

  def index: Action[AnyContent] = Action { implicit req =>
    Ok(views.html.index())
  }

  def welcome: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Ok(s"Welcome new user: ${ru.redditName}")
  }

}
