package services

import java.net.URLEncoder
import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import play.Environment
import play.api.Configuration
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}
import play.api.libs.ws.{WSAuthScheme, WSClient}
import play.api.mvc.{AnyContent, Request}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

@Singleton
class RedditService @Inject()(config: Configuration, env: Environment, ws: WSClient)(implicit ec: ExecutionContext) {

  private implicit val RedditUserInfoReads: Reads[RedditUserInfo] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "name").read[String] and
      (JsPath \ "has_verified_email").read[Boolean] and
      (JsPath \ "is_suspended").read[Boolean] and
      (JsPath \ "link_karma").read[Int] and
      (JsPath \ "comment_karma").read[Int] and
      (JsPath \ "created_utc").read[Long]
    ) ((id, name, isEmailVerified, isSuspended, linkKarma, commentKarma, creationTimestamp) => RedditUserInfo(id, s"/u/$name", isEmailVerified, isSuspended, linkKarma, commentKarma, creationTimestamp))

  private val clientId = config.get[String]("reddit.clientId")
  private val clientSecret = config.get[String]("reddit.clientSecret")

  def encodeRedirectUri(implicit req: Request[AnyContent]): String = URLEncoder.encode((if (env.isDev) "http://" else "https://") + req.host + "/auth/signin", "UTF-8")

  def oauthConsentUri(state: String)(implicit req: Request[AnyContent]): String = {
    s"https://www.reddit.com/api/v1/authorize/?client_id=$clientId&response_type=code&state=$state&redirect_uri=$encodeRedirectUri&duration=temporary&scope=identity"
  }

  def getIdentity(authCode: String)(implicit req: Request[AnyContent]): RedditUserInfo = {
    val accessTokenResponse = Await.result(
      ws.url("https://www.reddit.com/api/v1/access_token")
        .withAuth(clientId, clientSecret, WSAuthScheme.BASIC)
        .withMethod("POST")
        .withHttpHeaders("User-Agent" -> "web:com.duckblade.fakebaseball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Content-Type" -> "application/x-www-form-urlencoded")
        .post(s"grant_type=authorization_code&code=$authCode&redirect_uri=$encodeRedirectUri"),
      Duration(5, TimeUnit.SECONDS)
    )
    val accessToken = (accessTokenResponse.json \ "access_token").as[String]

    val myInfoResponse = Await.result(
      ws.url("https://oauth.reddit.com/api/v1/me")
        .withHttpHeaders("User-Agent" -> "web:com.duckblade.fakebaseball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Authorization" -> s"Bearer $accessToken")
        .get(),
      Duration(5, TimeUnit.SECONDS)
    )
    myInfoResponse.json.as[RedditUserInfo]
  }

}
