name := "api"

version := "1.0"

lazy val `api` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(guice, ws)

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.12",
  "com.typesafe.play" %% "play-slick" % "3.0.0"
)

unmanagedResourceDirectories in Test += {
  baseDirectory(_ / "target/web/public/test").value
}

// Reads config file for db migration and slick-codegen
import com.typesafe.config.{Config, ConfigFactory}

lazy val configFile = settingKey[Config]("Application configuration")
configFile := ConfigFactory.load(ConfigFactory.parseFile {
  val productionConf = new File("conf/production.conf")
  if (productionConf.exists)
    productionConf
  else {
    val env = System.getenv("config.file")
    if (env == null)
      new File("conf/development.conf")
    else
      new File(env)
  }
})

// Pre-Slick DB migration
import org.flywaydb.core.Flyway

lazy val migrate = taskKey[Unit]("Applies DB Migrations")
migrate := Flyway
  .configure()
  .locations("filesystem:conf/evolutions/")
  .dataSource(configFile.value.getString("slick.dbs.default.db.url"), configFile.value.getString("slick.dbs.default.db.user"), configFile.value.getString("slick.dbs.default.db.password"))
  .load()
  .migrate()

// Slick DB structure code generation, runs on every compile
lazy val codegen = taskKey[Unit]("Generates Slick tables")
codegen := slick.codegen.SourceCodeGenerator.main(Array(configFile.value.getString("slick.dbs.default.profile").replace("$", ""), configFile.value.getString("slick.dbs.default.db.driver"), configFile.value.getString("slick.dbs.default.db.url"), "./app/", "model", configFile.value.getString("slick.dbs.default.db.user"), configFile.value.getString("slick.dbs.default.db.password")))
(compile in Compile) := ((compile in Compile) dependsOn codegen).value
codegen := (codegen dependsOn migrate).value
